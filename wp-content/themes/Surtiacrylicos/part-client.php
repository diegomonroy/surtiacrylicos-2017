<!-- Begin Client -->
	<section class="client wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<h3 class="text-center">Quiénes han confiado en nosotros</h3>
				<?php echo do_shortcode( '[tc-owl-carousel carousel_cat="nuestros-clientes" order="ASC"]' ); ?>
			</div>
		</div>
	</section>
<!-- End Client -->