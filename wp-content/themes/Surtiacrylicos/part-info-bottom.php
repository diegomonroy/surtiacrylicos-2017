<!-- Begin Info Bottom -->
	<section class="info_bottom wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'info_bottom' ); ?>
			</div>
		</div>
	</section>
<!-- End Info Bottom -->