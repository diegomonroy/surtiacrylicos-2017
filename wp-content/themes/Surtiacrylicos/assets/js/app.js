// JavaScript Document

/* ************************************************************************************************************************

Surtiacrylicos

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {
	/* Menu */
	jQuery( 'ul.sub-menu' ).addClass( 'menu' );
	/* Newsletter */
	jQuery( '.tnp-email' ).attr( 'placeholder', 'Ingresa tu correo' );
	jQuery( 'input.tnp-submit' ).prop( 'value', 'ENVIAR' );
	/* Search */
	var searchBox = jQuery( '.moduletable_s1' );
	var searchBoxVar = searchBox.css( 'display' );
	var searchButton = jQuery( 'li#menu-item-17 a' );
	var searchClose = jQuery( 'li#menu-item-17 a.active_search' );
	if ( searchBoxVar == 'none' ) {
		searchButton.on('click', function ( event ) {
			event.preventDefault();
			searchBox.css( 'display', 'block' ).animate( { width: '240px', opacity: 1 }, 1000 );
			searchButton.addClass( 'active_search' );
		});
	}
	searchClose.on('click', function ( event ) {
		event.preventDefault();
		searchBox.css( 'display', 'none' ).animate( { width: '0', opacity: 0 }, 1000 );
		searchButton.removeClass( 'active_search' );
	});
});