<!-- Begin Top -->
	<section class="top wow fadeIn" data-wow-delay="0.5s">
		<div class="row align-justify align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-9 columns">
				<?php dynamic_sidebar( 'info' ); ?>
				<div class="clear"></div>
				<?php get_template_part( 'part', 'menu' ); ?>
				<?php dynamic_sidebar( 'search' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->