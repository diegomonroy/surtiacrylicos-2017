<!-- Begin Menu Left -->
	<div class="moduletable_le1">
		<h3 class="text-center">Productos</h3>
		<?php
		wp_nav_menu(
			array(
				'menu_class' => 'vertical menu',
				'container' => false,
				'theme_location' => 'left-menu',
				'items_wrap' => '<ul class="%2$s">%3$s</ul>'
			)
		);
		?>
	</div>
<!-- End Menu Left -->