<!-- Begin Content -->
	<section class="content wow fadeIn" data-wow-delay="0.5s">
		<div class="show-for-small-only">
			<button type="button" class="button side" data-toggle="offCanvas">PRODUCTOS</button>
			<div class="off-canvas-wrapper">
				<div class="off-canvas-absolute position-left" id="offCanvas" data-off-canvas>
					<button type="button" class="close-button" aria-label="Cerrar" data-close><span aria-hidden="true">&times;</span></button>
					<?php get_template_part( 'part', 'menu-left' ); ?>
				</div>
				<div class="off-canvas-content" data-off-canvas-content>
					<div class="row space">
						<div class="small-12 columns">
							<?php get_template_part( 'part', 'banner-mobile' ); ?>
							<?php get_template_part( 'part', 'content-main' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="hide-for-small-only">
			<div class="row space">
				<div class="medium-3 columns">
					<?php get_template_part( 'part', 'menu-left' ); ?>
				</div>
				<div class="medium-9 columns">
					<?php get_template_part( 'part', 'banner' ); ?>
					<?php get_template_part( 'part', 'content-main' ); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->