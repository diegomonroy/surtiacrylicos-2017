<div class="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; endif; ?>
</div>
<?php if ( is_front_page() || is_page( array( 'productos-destacados' ) ) ) : get_template_part( 'part', 'social-media' ); endif; ?>
<?php if ( is_front_page() || is_page( array( 'nuestra-empresa' ) ) ) : get_template_part( 'part', 'client' ); endif; ?>