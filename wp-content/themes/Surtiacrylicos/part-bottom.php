<!-- Begin Bottom -->
	<section class="bottom wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php get_template_part( 'part', 'menu-bottom' ); ?>
				<?php dynamic_sidebar( 'bottom' ); ?>
			</div>
		</div>
	</section>
<!-- End Bottom -->