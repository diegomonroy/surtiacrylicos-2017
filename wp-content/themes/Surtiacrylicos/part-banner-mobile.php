<!-- Begin Banner -->
	<section class="banner wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio_movil' ); endif; ?>
				<?php if ( is_page( array( 'nuestra-empresa' ) ) ) : dynamic_sidebar( 'banner_nuestra_empresa' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->